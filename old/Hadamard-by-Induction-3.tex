\documentclass[a4paper,12pt]{article}
\usepackage[utf8]{inputenc}
\usepackage{amsmath}
\usepackage{amssymb}

\usepackage{amsthm}
\usepackage{graphicx}
\graphicspath{ {./graphs/} }

\newtheorem{theo}{Theorem}
\newtheorem{prop}{Proposition}
\newtheorem{lem}{Lemma}
\newtheorem{cor}{Corollary}
\newtheorem*{defi}{Definition}

\def\id{\mathrm{Id}\,}
\def\dom{\mathrm{dom}\,}
\def\gr{\mathrm{Gr}\,}
\def\R{\mathbb{R}}
%opening
\title{Hopefully New Proof of Hadamard Implicit Function Theorem}
\author{draft}

\begin{document}

\maketitle

\begin{abstract}
  Hadamard Global Inverse Function Theorem by the Methods of Variational Analysis.
\end{abstract}

\section{Introduction}

Let $X$ be a Banach spaces. Recall that the function 
  $$
    f: X\to X
  $$
  is called Fr\'echet differentiable at $x\in X$ if there is a bounded linear $f'(x):X\to X$ such that
  $$
    \lim_{\|h\|\to 0}\frac{f(x+h)-f(x)-f'(x)h}{\|h\|} = 0.
  $$
  The function $f$ is called {\em smooth}, denoted $f\in C^1$, if the function
  $$
    x \to f'(x)
  $$
  is norm-to-norm continuos.
  
We present a  proof to the following classical

\begin{theo}
  \label{thm:main}
 Let $X$ be a Banach space. Let $f\in C^1$ and $f'(x)$ be invertible for all $x$ and satisfying:
   \begin{equation}
    \label{eq:2}
    \|[f'(x)]^{-1}\| \le L,\quad\forall x\in X,
  \end{equation}
  for some $L > 0$.


 Then  $f$ is $C^1$ invertible on $X$.
  
  In other words, there is $g\in C^1$ such that 
  $$
     g(f(x)) =f(g(x))=x,\quad\forall x\in X.
  $$  
\end{theo}

\section{Preliminaries}

We start with recalling the classical (local) Inverse Function Theorem.
\begin{theo}
 \label{thm:inverse-classical}
  Let $f\in C^1$ and let $f'(x_0)$ be invertible. Then there are $\varepsilon,\delta >0$ such that for each $y$ such that
  $$
     \|y - f(x_0)\| <\varepsilon
  $$
  there is unique $x=:g(y)$ such that $\|x-x_0\| < \delta$ and
  $$
    f(x) = y.
  $$
  Moreover, $g\in C^1$ and 
  $$
    g'(f(x_0)) = [f'(x_0)]^{-1}.
  $$
\end{theo}

\begin{cor}
  \label{cor:unique}
  Let $f\in C^1$ and let $f'(x_0)$ be invertible. 
  
  Let $L>0$.
  
  There is $\nu > 0$ such that if  $g_1$ and $g_2$ are $L$-Lipschitz  and such that 
  $$
     g_1(f(x_0)) = g_2(f(x_0)) = x_0,
  $$
  and 
  $$
     f(g_1(y)) = f(g_2(y)) = y,\quad\forall y:\ \|y-f(x_0)\| < \nu,
  $$
   then
  $$
    g_1(y) = g_2(y),\quad\forall y: \ \|y-f(x_0)\| < \nu.
  $$
\end{cor}
\begin{proof}
 In view of Theorem~\ref{thm:inverse-classical} it is enough to take $\nu$ so small that
 $$
   \nu < \varepsilon\mbox{ and } L\nu < \delta.
 $$
\end{proof}

Recall that in a normed space $(X,\|\cdot\|)$ the Kuratowski distance between $\varnothing\neq A\subset B\subset X$ can be defined as
$$
  d(A,B) := \sup\{ d(x,A):\ x\in B\},
$$
where
$$
  d(x,A) := \inf \{\|x-a\|:\ a\in A\}.
$$

\begin{lem}
  \label{lem-tn-0}
  Let $K$ be a compact subspace of the normed space $(X,\|\cdot\|)$. If $(A_n)_1^\infty$ is a nested, that is,
  $$
     A_n \subset A_{n+1},\quad\forall n\in\mathbb{N},
  $$
  sequence of nonempty subsets of $K$ then
  $$
     \lim_{n\to\infty} d(A_n,A_{n+1}) = 0.
  $$
\end{lem}
\begin{proof}
 Assume that there are $r>0$ and $n_k\in\mathbb{N}$, $n_k <n_{k+1}$, $\forall k$ such that
 $$
    d(A_{n_k-1},A_{n_k}) > r,\quad\forall k\in\mathbb{N}.
 $$
 Since $d(A_{n_{k-1}},A_{n_k}) > d(A_{n_k-1},A_{n_k})$, we have
 $$
    d(A_{n_{k-1}},A_{n_k})  > r,\quad\forall k\in\mathbb{N}.
 $$
 So, there are $x_k\in A_{n_k}$ such that $d(x_k, A_{n_{k-1}}) > r$. But, since $x_m\in A_{n_{k-1}}$ for $m<k$, we have
 $$
   \|x_m - x_k\| > r, \quad\forall m < k,
 $$
 which in efect means
 $$
   \|x_m - x_k\| > r, \quad\forall m \neq k,
 $$
 and there is no convergent subsequence to $(x_k)_1^\infty$.
\end{proof}

\begin{prop}
   \label{pro:extent}
   Let $L > 0$.
   Let $(X,\|\cdot\|)$ be a normed space and let
   $$
     f:X\to X
   $$
   be a continuous function.  Let $K\subset X$ be nonempty  and compact.
   
   Consider the family $\mathcal{B}$ consisting of couples $(B,g_B)$, where $B\subset K$ is nonempty and compact,  and $g_B:B\to X$ is $L$-Lipschitz and such that
   $$
     f(g(y)) = y,\quad\forall y \in B.
   $$
   For $(B_i,g_{B_i})\in\mathcal{B}$ we write
   $$
     (B_1,g_{B_1}) \prec(B_2,g_{B_2})\iff B_1\subset B_2\mbox{ and }g_{B_2}(y) = g_{B_1}(y),\ \forall y\in B_1.
   $$
   
   Assume that for each $(B,g_B) \in \mathcal{B}$ such that $B\subsetneqq K$, there is $(B',g_{B'}) \in \mathcal{B}$ such that $(B,g_B)\prec (B',g_{B'})$ and $B\subsetneqq B'$.
   
   Then if $(A,g_A) \in \mathcal{B}$ there is $(K,g_{K}) \in \mathcal{B}$ such that
   $$
      (A,g_A) \prec (K,g_K).
   $$
\end{prop}

\begin{proof}
   Let $A_1=A$. If $(A_n,g_{A_n})\in\mathcal{B}$ is chosen and $A_n\subsetneqq K$, then let
   \begin{equation}
     \label{eq:def-tn}
     t_n: = \sup \{ d(A_n,A'): \ (A',g_{A'})\in\mathcal{B}\mbox{ and }(A_n,g_{A_n})\prec (A',g_{A'})\}.
   \end{equation}

   By assumption $t_n > 0$. Also, $t_n < \infty$, since $K$ is bounded.
   
   Take $(A_{n+1},g_{A_{n+1}})\in\mathcal{B}$ such that $(A_n,g_{A_n})\prec (A_{n+1},g_{A_{n+1}})$ and
   \begin{equation}
      \label{eq:an+1-choice}
      d(A_n,A_{n+1}) > t_n/2.
   \end{equation}
   
   If at some step $A_n= K$ we are done. Otherwise by Lemma~\ref{lem-tn-0} we have $d(A_n,A_{n+1})\to 0$ as $n\to\infty$ and so, by \eqref{eq:an+1-choice}:
   \begin{equation}
      \label{eq:tn-to-0}
      \lim _{n\to\infty} t_n = 0.
   \end{equation}
   
   Let 
   $$
     B := \bigcup_{n=1}^\infty A_n,\quad g_B(y) = g_{A_n}(y),\mbox{ if }y\in A_n.
   $$
   It is easy to check that $g_B$ is well defined.
   
   Since $g_B$ is $L$-Lipschitz, it can be extended by continuity to the closure $\overline{B}$ of $B$. We denote this  $L$-Lipschitz extension by $g_{\overline{B}}$. Since $f$ is continuous, $f(g(y))=y$ for all $y\in \overline{B}$. In other words, $(\overline{B},g_{\overline{B}})\in\mathcal{B}$. Clearly, $(A_n,g_{A_n})\prec (\overline{B},g_{\overline{B}})$ for all $n$.
   
   If $\overline{B} = K$ we are done. Otherwise by assumption there is $(B',g_{B'})\in\mathcal{B}$ such that $(\overline{B},g_{\overline{B}}) \prec (B',g_{B'})$ and $\overline{B}\subsetneqq B'$. By the definition \eqref{eq:def-tn}
   $$
     t_n \ge d(A_n, B') > 0,\quad\forall n\in\mathbb{N},
   $$
   which contradicts \eqref{eq:tn-to-0}.
\end{proof}

The following is a precursor to Ekeland Variational Principle, see \cite{ek-tem}. Of course, it easily follows from Ekeland Variational Principle.
\begin{lem}
  \label{lem-ek-tem}
  Let $X$ be a Banach space and let $\mu:X \to \R^+\cup\{\infty\}$ be lower semicontinuous and such that for some $ r>0$
  $$
    \forall x:\ 0<\mu (x) < \infty\Rightarrow \exists y:\ \mu(y) < \mu(x) - r\|y-x\|.
  $$
  Then for each $x\in \dom \mu$ there is $y\in X$ such that
  $$
     \mu(y) = 0\mbox{ and }r\|y-x\| \le \mu(x).
  $$
\end{lem}
\begin{proof}
  Fix $x_0\in \dom \mu$ such that $\mu(x_0) > 0$.  Let $x_1,x_2,\ldots,x_n$ be already chosen in the following way.

  
  Let 
  \begin{equation}
   \label{eq:nu-def}
     \nu_n= \sup \{ \|x-x_n\|:\ \mu(x) < \mu(x_n) - r\|x-x_n\|\}.
  \end{equation}
  We are given that the set in the right hand side is nonempty, so $\nu_n > 0$. Also, since $\mu\ge 0$, we have that $\nu_n \le  \mu(x_n)/r <\infty$.

  Choose a $x_{n+1}$ such that
  \begin{equation}
   \label{eq:x-n+1-def}
     \mu(x_{n+1}) < \mu(x_n) - r\|x_{n+1}-x_n\|
     \mbox{ and }\|x_{n+1}-x_n\| > \nu_n/2.
  \end{equation}
  Note that 
  $$
    \|x_{n+1} - x_0\| \le \sum_{i=0}^n \|x_{i+1}-x_i\|\le \sum_{i=0}^n(\mu(x_i)-\mu(x_{i+1}))/r\le\mu(x_0)/r,
  $$
  so: if $\mu(x_{n+1})=0$, we are done. If not, we continue by induction.
  
  If we would end up with an infinite sequence $(x_n)_0^\infty$, then from the above inequality $\sum_{i=0}^\infty \|x_{i+1}-x_i\| \le\mu(x_0)/r$, so $x_n\to \bar x$ as $n\to\infty$ and $\|\bar x- x_0\|\le \mu(x_0)/r$.
  
  If $\mu(\bar x) > 0$ then we can find $\bar y$ such that $\mu(\bar y) < \mu(\bar x) - r\|\bar y-\bar x\|$. Since $\mu$ is lower semicontinuous, we will have for all $n$ large enough $\mu(\bar y) < \mu(x_n) - r\|\bar y-x_n\|$. That is, see \eqref{eq:nu-def},  $\nu_n \ge \|\bar y-x_n\| \ge  \|\bar y-\bar x\|/2 > 0$ for all $n$ large enough. But on the other hand from \eqref{eq:x-n+1-def} it follows that $\nu_n\to 0$, contradiction.
  
  So,  $\mu(\bar x) = 0$ and we are done.
\end{proof}

We make a crucuial use of the following Theorem of Ekeland, \cite{ek:article}
\begin{theo}
 \label{thm:ekeland-surj}
  Let $X$ be a Banach space. Let $f:X\to X$ be continuous G\^ateaux differentiable and satisfying \eqref{eq:2}. Then 
  \begin{equation}
     \label{eq:open-linear}
     f(B^\circ(x;Lr)) \supset B^\circ(f(x);r),\quad\forall x\in X,\forall r > 0.
  \end{equation}
\end{theo}
\begin{proof}
We can assume without loss of generality that $f(0)=0$.

 Fix a $\bar y \in X$ such that $\|\bar y\| < r$ and consider
 $$
   \mu(x) := \|f(x) - \bar y\|.
 $$
 
 Fix $a\in (0,1)$ such that $\|\bar y\| /a < r$.
 
 If $\mu(x) >0$, that is $f(x)\neq \bar y$, then $h:=[f'(x)]^{-1}(\bar y - f(x))\neq 0$. Since
 \begin{eqnarray*}
   f(x+th) &=& f(x) + tf'(x)h + o(t)\\
                 &=& f(x) + t(\bar y -f(x)) + o(t),
 \end{eqnarray*}
 we have that
  \begin{eqnarray*}
    \mu(x+th) &=& \|f(x+th) - \bar y\|\\
                        &=& (1-t)\mu(x) + o(t)\\
                        &<& (1-at)\mu(x), \quad t\in (0,\delta)
  \end{eqnarray*}
  for some $\delta>0$. 
  
  Since $\|h\| = \|[f'(x)]^{-1}(\bar y - f(x))\| \le \|[f'(x)]^{-1}\| \|\bar y - f(x)\| \le L\mu(x)$, we have
  $$
     \mu(x+th) < \mu(x) - (a/L)\|th\|
  $$
  for all $t\in (0,\delta)$. That is, $\mu(y) < \mu(x) - (a/L)\|y-x\|$ for $y = x+th$. From Lemma~\ref{lem-ek-tem} it follows that there is $\bar x$ such that $\mu(\bar x) = 0$ and $\|\bar x\| \le (L/a)\|\bar y\|$. But the latter is smaller than $Lr$.
  
  In short, we have proved that for each $\bar y$ such that  $\|\bar y\| < r$  there is $\bar x$ such that $\|\bar x \| < Lr$ and $f(\bar x) = \bar y$.
\end{proof}




\section{Proof of Theorem~\ref{thm:main}}
  
Let $X$ be a separable Banach space and let $f:X\to X$ be $C^1$-smooth and satisfying \eqref{eq:2}. Assume that 
$$
  f(0) = 0.
$$

We sill be using small letters for functions, or singlevalued mappings, and caps for multivalued mappings. For $G_1$ and $G_2$ we wite $G_1\subset G_2$ if $\gr G_1 \subset \gr G_2$.

Define (and fix) 
$$
  G(y) := \{x\in X:\ f(x) = y\}.
$$
From Ekeland's Surjectivity Therorem~\ref{thm:ekeland-surj} it follows that
\begin{equation}
  \label{eq:G-noempty}
  G(y) \neq\varnothing,\quad\forall y\in X.
\end{equation}
Actually, if we are bit more careful with the proof, we can get the opennes at linear rate of $f$, that is, $f(B(x;r))\supset B(f(x);r/M)$,  expressed in terms of $G$.
\begin{lem}
  \label{lem:aubin}
  If $x_0\in G(y_0)$ then 
  \begin{equation}
     \label{eq:aubin}
     G(y) \cap B(x_0; M\|y-y_0\|)  \neq\varnothing,\quad\forall y\in X.
  \end{equation}
\end{lem}
\begin{proof}
  Consider again $\mu(x) = \|f(x)-\bar y\|$ for some fixed $\bar y$. We get the same way that there is $\bar x$ such that $\mu(\bar x) = 0$ and $\|\bar x - x_0\| \le ?\mu(x_0)$.
\end{proof}


The greater part of the work below will be to show that there is $L$-Lipschitz $g:X\to X$ such that $g(0)=0$ and $g\subset G$. We need this because of the following
\begin{lem}
  \label{lem:inj-0}
  If there is continuos $g:X\to X$ such that 
  $$
    g(0)=0\mbox{ and }g\subset G,
  $$
  then
  $$
    f(x)\neq 0,\quad\forall x\neq 0.
  $$
\end{lem}
\begin{proof}
 Fix $\bar x \neq 0$ and let
 $$
   \varphi (t) := f(t\bar x),\quad t\in[0,1]. 
 $$
 Define 
 $$
   I:= \{ t\in [0,1]:\ g(\varphi(t)) = \bar x\}.
 $$
 It is clear that $0\in I$, so the latter is nonempty. Also, $I$ is closed by continuity.
 
 Let $\bar t := \max I$. If we assume that $\bar t < 1$ then for $s$ small enough...
 
 (contradiction with the local uniqueness)
 
 So, $g(f(\bar x)) = \bar x\neq 0$. Thus $f(\bar x)\neq 0$, because $g(0)=0$.
\end{proof}



\begin{thebibliography}{99}
   \bibitem{ek-tem} book of Ekeland and Temmam, p.?
   \bibitem{ek:article} Ekeland's article
\end{thebibliography}


\end{document}
