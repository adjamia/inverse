\documentclass[a4paper,12pt]{article}
\usepackage[utf8]{inputenc}
\usepackage{amsmath}
\usepackage{amssymb}

\usepackage{amsthm}
\usepackage{graphicx}
\graphicspath{ {./graphs/} }

\newtheorem{theo}{Theorem}
\newtheorem{prop}{Proposition}
\newtheorem{lem}{Lemma}
\newtheorem{cor}{Corollary}
\newtheorem*{defi}{Definition}

\def\id{\mathrm{Id}\,}
\def\dom{\mathrm{dom}\,}
\def\R{\mathbb{R}}
%opening
\title{Hopefully New Proof of Hadamard Implicit Function Theorem}
\author{draft}

\begin{document}

\maketitle

\begin{abstract}
  Hadamard Global Inverse Function Theorem by the Methods of Variational Analysis.
\end{abstract}

\section{Introduction}

Let $X$ be a Banach spaces. Recall that the function 
  $$
    f: X\to X
  $$
  is called Fr\'echet differentiable at $x\in X$ if there is a bounded linear $f'(x):X\to X$ such that
  $$
    \lim_{\|h\|\to 0}\frac{f(x+h)-f(x)-f'(x)h}{\|h\|} = 0.
  $$
  The function $f$ is called {\em smooth}, denoted $f\in C^1$, if the function
  $$
    x \to f'(x)
  $$
  is norm-to-norm continuos.
  
We present a  proof to the following classical

\begin{theo}
  \label{thm:main}
 Let $X$ be separable. Let $f\in C^1$ and $f'(x)$ be invertible for all $x$ and satisfying:
  \begin{equation}
   \label{eq:1}
   \|f'(x)\| \le L,\quad\forall x\in X,
  \end{equation}
  \begin{equation}
    \label{eq:2}
    \|[f'(x)]^{-1}\| \le L,\quad\forall x\in X,
  \end{equation}
  for some $L > 0$.


 Then  $f$ is $C^1$ invertible on $X$.
  
  In other words, there is $g\in C^1$ such that 
  $$
     g(f(x)) =f(g(x))=x,\quad\forall x\in X.
  $$  
\end{theo}

\section{Preliminaries}

We start with recalling the classical (local) Inverse Function Theorem.
\begin{theo}
 \label{thm:inverse-classical}
  Let $f\in C^1$ and let $f'(x_0)$ be invertible. Then there are $\varepsilon,\delta >0$ such that for each $y$ such that
  $$
     \|y - f(x_0)\| <\varepsilon
  $$
  there is unique $x=:g(y)$ such that $\|x-x_0\| < \delta$ and
  $$
    f(x) = y.
  $$
  Moreover, $g\in C^1$ and 
  $$
    g'(f(x_0)) = [f'(x_0)]^{-1}.
  $$
\end{theo}

\begin{cor}
  \label{cor:unique}
  Let $f\in C^1$ and let $f'(x_0)$ be invertible. 
  
  Let $L>0$.
  
  There is $\nu > 0$ such that if  $g_1$ and $g_2$ are $L$-Lipschitz  and such that 
  $$
     g_1(f(x_0)) = g_2(f(x_0)) = x_0,
  $$
  and 
  $$
     f(g_1(y)) = f(g_2(y)) = y,\quad\forall y:\ \|y-f(x_0)\| < \nu,
  $$
   then
  $$
    g_1(y) = g_2(y),\quad\forall y: \ \|y-f(x_0)\| < \nu.
  $$
\end{cor}
\begin{proof}
 In view of Theorem~\ref{thm:inverse-classical} it is enough to take $\nu$ so small that
 $$
   \nu < \varepsilon\mbox{ and } L\nu < \delta.
 $$
\end{proof}

Recall that in a normed space $(X,\|\cdot\|)$ the Kuratowski distance between $\varnothing\neq A\subset B\subset X$ can be defined as
$$
  d(A,B) := \sup\{ d(x,A):\ x\in B\},
$$
where
$$
  d(x,A) := \inf \{\|x-a\|:\ a\in A\}.
$$

\begin{lem}
  \label{lem-tn-0}
  Let $K$ be a compact subspace of the normed space $(X,\|\cdot\|)$. If $(A_n)_1^\infty$ is a nested, that is,
  $$
     A_n \subset A_{n+1},\quad\forall n\in\mathbb{N},
  $$
  sequence of nonempty subsets of $K$ then
  $$
     \lim_{n\to\infty} d(A_n,A_{n+1}) = 0.
  $$
\end{lem}
\begin{proof}
 Assume that there are $r>0$ and $n_k\in\mathbb{N}$, $n_k <n_{k+1}$, $\forall k$ such that
 $$
    d(A_{n_k-1},A_{n_k}) > r,\quad\forall k\in\mathbb{N}.
 $$
 Since $d(A_{n_{k-1}},A_{n_k}) > d(A_{n_k-1},A_{n_k})$, we have
 $$
    d(A_{n_{k-1}},A_{n_k})  > r,\quad\forall k\in\mathbb{N}.
 $$
 So, there are $x_k\in A_{n_k}$ such that $d(x_k, A_{n_{k-1}}) > r$. But, since $x_m\in A_{n_{k-1}}$ for $m<k$, we have
 $$
   \|x_m - x_k\| > r, \quad\forall m < k,
 $$
 which in efect means
 $$
   \|x_m - x_k\| > r, \quad\forall m \neq k,
 $$
 and there is no convergent subsequence to $(x_k)_1^\infty$.
\end{proof}

\begin{prop}
   \label{pro:extent}
   Let $L > 0$.
   Let $(X,\|\cdot\|)$ be a normed space and let
   $$
     f:X\to X
   $$
   be a continuous function.  Let $K\subset X$ be nonempty  and compact.
   
   Consider the family $\mathcal{B}$ consisting of couples $(B,g_B)$, where $B\subset K$ is nonempty and compact,  and $g_B:B\to X$ is $L$-Lipschitz and such that
   $$
     f(g(y)) = y,\quad\forall y \in B.
   $$
   For $(B_i,g_{B_i})\in\mathcal{B}$ we write
   $$
     (B_1,g_{B_1}) \prec(B_2,g_{B_2})\iff B_1\subset B_2\mbox{ and }g_{B_2}(y) = g_{B_1}(y),\ \forall y\in B_1.
   $$
   
   Assume that for each $(B,g_B) \in \mathcal{B}$ such that $B\subsetneqq K$, there is $(B',g_{B'}) \in \mathcal{B}$ such that $(B,g_B)\prec (B',g_{B'})$ and $B\subsetneqq B'$.
   
   Then if $(A,g_A) \in \mathcal{B}$ there is $(K,g_{K}) \in \mathcal{B}$ such that
   $$
      (A,g_A) \prec (K,g_K).
   $$
\end{prop}

\begin{proof}
   Let $A_1=A$. If $(A_n,g_{A_n})\in\mathcal{B}$ is chosen and $A_n\subsetneqq K$, then let
   \begin{equation}
     \label{eq:def-tn}
     t_n: = \sup \{ d(A_n,A'): \ (A',g_{A'})\in\mathcal{B}\mbox{ and }(A_n,g_{A_n})\prec (A',g_{A'})\}.
   \end{equation}

   By assumption $t_n > 0$. Also, $t_n < \infty$, since $K$ is bounded.
   
   Take $(A_{n+1},g_{A_{n+1}})\in\mathcal{B}$ such that $(A_n,g_{A_n})\prec (A_{n+1},g_{A_{n+1}})$ and
   \begin{equation}
      \label{eq:an+1-choice}
      d(A_n,A_{n+1}) > t_n/2.
   \end{equation}
   
   If at some step $A_n= K$ we are done. Otherwise by Lemma~\ref{lem-tn-0} we have $d(A_n,A_{n+1})\to 0$ as $n\to\infty$ and so, by \eqref{eq:an+1-choice}:
   \begin{equation}
      \label{eq:tn-to-0}
      \lim _{n\to\infty} t_n = 0.
   \end{equation}
   
   Let 
   $$
     B := \bigcup_{n=1}^\infty A_n,\quad g_B(y) = g_{A_n}(y),\mbox{ if }y\in A_n.
   $$
   It is easy to check that $g_B$ is well defined.
   
   Since $g_B$ is $L$-Lipschitz, it can be extended by continuity to the closure $\overline{B}$ of $B$. We denote this  $L$-Lipschitz extension by $g_{\overline{B}}$. Since $f$ is continuous, $f(g(y))=y$ for all $y\in \overline{B}$. In other words, $(\overline{B},g_{\overline{B}})\in\mathcal{B}$. Clearly, $(A_n,g_{A_n})\prec (\overline{B},g_{\overline{B}})$ for all $n$.
   
   If $\overline{B} = K$ we are done. Otherwise by assumption there is $(B',g_{B'})\in\mathcal{B}$ such that $(\overline{B},g_{\overline{B}}) \prec (B',g_{B'})$ and $\overline{B}\subsetneqq B'$. By the definition \eqref{eq:def-tn}
   $$
     t_n \ge d(A_n, B') > 0,\quad\forall n\in\mathbb{N},
   $$
   which contradicts \eqref{eq:tn-to-0}.
\end{proof}

\section{Proof of Theorem~\ref{thm:main}}
Consider the family $\mathcal{B}$ of couples $(B,g_B)$ of nonemtpy compact and convex $B\subset X$ and $L$-Lipschitz functions $g_B:B\to X$ such that
$$
  f(g_B(y)) = y,\quad\forall y\in B,
$$
with the same partial ordering as in Proposition~\ref{pro:extent}.
\begin{lem}
 \label{lem:extent}
 If $\varnothing\neq A\subset K$ are compact and convex and there is $(A,g_A)\in\mathcal{B}$, then there exists $(K,g_K)\in\mathcal{B}$ such that
 $$
   (A,g_A) \prec (K,g_K).
 $$
\end{lem}
\begin{proof}
  We will check the conditions of Proposition~\ref{pro:extent}.
  
  Let $(B,g_B)\in\mathcal{B}$ be such that $B\subsetneqq K$. 
  
  
\end{proof}




\end{document}
