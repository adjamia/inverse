\documentclass[a4paper,12pt]{article}
\usepackage[utf8]{inputenc}
\usepackage{amsmath}
\usepackage{amssymb}

\usepackage{amsthm}
\usepackage{graphicx}
\graphicspath{ {./graphs/} }

\newtheorem{theo}{Theorem}
\newtheorem{prop}{Proposition}
\newtheorem{lem}{Lemma}
\newtheorem*{defi}{Definition}

\def\id{\mathrm{Id}\,}
\def\dom{\mathrm{dom}\,}
\def\R{\mathbb{R}}
%opening
\title{Inverse Function Theorem for G\^ateaux smooth function} 
\author{Milen Ivanov and Nadia Zlateva}
\date{}

\begin{document}

\maketitle

\begin{abstract}
 We prove an Inverse Function Theorem for G\^ateaux smooth function adapting an idea of I. Ekeland and E. S\'er\'e~\cite{ES}.
\end{abstract}

\section{Introduction} 

Let $X$ be a Banach space. Consider a function
$$
    f: X\to X.
  $$
Recall that its directional derivative $  f'(x;h)$ at the point $x$ in the direction $h$ is 
$$
    f'(x;h) := \lim_{t\to 0}\frac{f(x+th) - f(x)}{t}. 
  $$
 The function $f$  is called G\^ateaux differentiable at $x\in X$ if there is a bounded linear $f'(x):X\to X$ such that
  $$
    f'(x;h) = f'(x)h.
  $$
  The function $f$ is called {\em G\^ateaux smooth}, denoted $f\in C_G^1$, if in addition the function
  $$
   (x,h) \to f'(x;h)
  $$
  is continuous.
  
  
  

The following Theorem, known as Hadamard Theorem, is well known for Fr\'echet smooth $f$. The aim of this work is to extend Hadamard Theorem to the case of G\^ateaux smooth function. Here is the exact statement.
\begin{theo}
  \label{thm:main}
 Let $X$ be a Banach space. Let $f\in C_G^1$ and let $f'(x)$ be invertible for all $x$. Let also 
   \begin{equation}
    \label{eq:2}
    \|[f'(x)]^{-1}\| \le \frac{1}{r},\quad\forall x\in X,
  \end{equation}
  for some $r > 0$; and let the map
   \begin{equation}
    \label{eq:3}
    (x,h) \to [f'(x)]^{-1}h
  \end{equation}
  be continuous.


 Then   there is $g\in C^1_G$ which is $r^{-1}$-Lipschitz and such that
\begin{equation}
   \label{eq:g-is-inverse}
     g(f(x)) =f(g(x))=x,\quad\forall x\in X.
  \end{equation}
  Moreover,
  \begin{equation}
   \label{eq:inv-der} 
   g'(f(x)) = [f'(x)]^{-1},\quad\forall x\in X.
  \end{equation}
\end{theo}
Note that the only new part in this Theorem is actually the fact that $f$ is injective and proving this makes for the bulk of the tough technical work below.

\section{Preliminaries}
\label{sec:prelim}
\begin{lem}
  \label{lem:hadam-der}
  Let $f$ be $C_G^1$-smooth and let $(x_n,h_n,t_n)\to (\bar x, \bar h, 0)$ as $n\to\infty$. Then
  $$
    \lim_{n\to\infty} {f(x_n + t_nh_n) - f(x_n) \over t_n} = f'(\bar x,\bar h).
  $$
\end{lem}
\begin{proof}
  Assume the contrary, that is there $\varepsilon > 0$ and sequences $(x_n,h_n,t_n)\to (\bar x, \bar h, 0)$ such that
  \begin{equation}
    \label{eq:hadam-der-1}
    \left\| {f(x_n + t_nh_n) - f(x_n) \over t_n} - f'(\bar x,\bar h)\right\| > \varepsilon.
  \end{equation}


   Fix a $n\in\mathbb{N}$ and let
   $$
     \varphi(s) := f(x_n+st_nh_n) \Rightarrow  \varphi'(s) = t_n f'(x_n+st_nh_n;h_n).
   $$
   Then
   \begin{eqnarray*}
      f(x_n+st_nh_n) - f(x_n) &=& \varphi(1) - \varphi(0)\\
      = \int_0^1 \varphi'(s)\, ds &=& t_n \int _0^1 f'(x_n+st_nh_n;h_n)\, ds.
   \end{eqnarray*}
  So, from \eqref{eq:hadam-der-1} it follows that 
  \begin{eqnarray*}
     \varepsilon &<& \left\| {f(x_n + t_nh_n) - f(x_n) \over t_n} - f'(\bar x,\bar h)\right\|\\
     &=& \left\| \int_0^1 f'(x_n+st_nh_n;h_n) -  f'(\bar x,\bar h)\, ds\right\|\\
     &\le& \int_0^1 \|f'(x_n+st_nh_n;h_n) -  f'(\bar x,\bar h)\|\, ds.
  \end{eqnarray*}
  Therefore, there is $\xi_n\in[x_n,x_n+t_nh_n]$ such that
  $$
    \|f'(\xi_n, h_n) -  f'(\bar x,\bar h)\| >\varepsilon >0.
  $$
  But $\xi_n\to\bar x$, contradiction.
\end{proof}
\begin{lem} 
   \label{lem:uniform-on-compact}
   Let $f$ be $C^1_G$. Let $K\subset X$ be compact. Then for any $\varepsilon>0$ there are $\mu,\delta>0$ such that for all $a\in K$ and  $x\in B(a;\mu)$     
  $$
    \| f(x+th) - f(x) - tf'(a)h\| < \varepsilon t,\quad \forall h\in K,\ \forall t\in(0,\delta).
  $$
\end{lem}
\begin{proof}
 Assume the contrary. That is, there are $(a_n)_1^\infty\subset K$, $x_n\in X$ such that $\|x_n-a_n\|\to 0$ as $n\to\infty$, $h_n\in K$ and $t_n\searrow 0$ as $n\to\infty$ such that
 $$
   \|f(x_n + t_nh_n) - f(x_n) - t_nf'(a_n;h_n)\| \ge t_n\varepsilon ,
 $$
 or, which is the same,
 \begin{equation}
   \label{eq:uniform-on-compact:1}
   \left\| {f(x_n + t_nh_n) - f(x_n) \over t_n} - f'(a_n;h_n)\right\| \ge \varepsilon.
 \end{equation}
 Since $K$ is compact, we can assume without loss of generality that $a_n\to\bar a$, so $x_n\to \bar a$; and $h_n\to \bar h$. Then Lemma~\ref{lem:hadam-der} gives $t_n^{-1}(f(x_n + t_nh_n) - f(x_n))\to f'(\bar a; \bar h)$, while $f'(a_n;h_n)\to f'(\bar a; \bar h)$ by continuity; and \eqref{eq:uniform-on-compact:1} yeilds a contradiction.
\end{proof}
 
Next is a precursor to Ekeland Variational Principle, see \cite[Chapter 5, Section 1]{AE}. Of course, it easily follows from Ekeland Variational Principle itself, see e.g. \cite[Basic Lemma]{ioffe}. See also the comments concerning the ``Basic  Lemma'' on \cite[p. 93]{ioffe}. Here we present a proof based on what is called in these comments ``simple iteration''.
\begin{lem}
  \label{lem-ek-tem}
  Let $X$ be a Banach space and let $\mu:X \to \R^+\cup\{\infty\}$ be lower semicontinuous and such that for some $ r>0$
  $$
    \forall x:\ 0<\mu (x) < \infty\Rightarrow \exists y:\ \mu(y) < \mu(x) - r\|y-x\|.
  $$
  Then for each $x\in \dom \mu$ there is $y\in X$ such that
  $$
     \mu(y) = 0\mbox{ and }r\|y-x\| \le \mu(x).
  $$
\end{lem}

\begin{proof}
  Fix $x_0\in \dom \mu$ such that $\mu(x_0) > 0$.  Let $x_1,x_2,\ldots,x_n$ be already chosen in the following way.


Set
  \begin{equation}
   \label{eq:nu-def}
     \nu_n= \sup \{ \|x-x_n\|:\ \mu(x) < \mu(x_n) - r\|x-x_n\|\}.
  \end{equation}
  We are given that the set in the right hand side is nonempty, so $\nu_n > 0$. Also, since $\mu\ge 0$, we have that $\nu_n \le  \mu(x_n)/r <\infty$.

  Choose a $x_{n+1}$ such that
  \begin{equation}
   \label{eq:x-n+1-def}
     \mu(x_{n+1}) < \mu(x_n) - r\|x_{n+1}-x_n\|
     \mbox{ and }\|x_{n+1}-x_n\| > \nu_n/2.
  \end{equation}
  Note that
  $$
    \|x_{n+1} - x_0\| \le \sum_{i=0}^n \|x_{i+1}-x_i\|\le \sum_{i=0}^n(\mu(x_i)-\mu(x_{i+1}))/r\le\mu(x_0)/r.
  $$
If  if $\mu(x_{n+1})=0$, we are done. If not, we continue by induction.

  If we would end up with an infinite sequence $(x_n)_0^\infty$, then from the above inequality $\sum_{i=0}^\infty \|x_{i+1}-x_i\| \le\mu(x_0)/r$, so $x_n\to \bar x$ as $n\to\infty$ and $\|\bar x- x_0\|\le \mu(x_0)/r$.  From \eqref{eq:x-n+1-def} it follows that $\nu_n\to 0$.


  If $\mu(\bar x) > 0$ then we can find $\bar y$ such that
  \begin{equation}\label{vuh}
  \mu(\bar y) < \mu(\bar x) - r\|\bar y-\bar x\|.
   \end{equation}
   Since $\mu$ is lower semicontinuous, we will have for all $n$ large enough $\mu(\bar y) < \mu(x_n) - r\|\bar y-x_n\|$. Hence, see \eqref{eq:nu-def},  $\nu_n \ge \|\bar y-x_n\| $ for all $n$ large enough. Since $\nu_n\to 0$, we get that $\bar y =\bar x$ which contradicts \eqref{vuh}.

  So,  $\mu(\bar x) = 0$ and we are done.
\end{proof}



\section{Right inverse \`a la Ekeland \& S\'er\'e}
\label{sec:mainpro}
If $K\subset X$ is compact then $C(K,X)$ is the space of all continuous functions from $K$ to $X$. It is clear that with the norm
$$
   \|g\|_\infty := \max_{y\in K} \|g(y)\|
$$
it is a Banach space.

\begin{prop}
 \label{pro:main}
  Let $f\in C_G^1$,  $f'(x)$ be invertible for all $x$ and let $f$ satisfy~\eqref{eq:2} and \eqref{eq:3}. Let $K\subset X$ be compact. Then $f$ has a continuous right inverse on $K$, that is, there is $g\in C(K,X)$ such that
  $$
    f(g(x)) = x,\quad \forall x\in K.
  $$
  Moreover, if $A\subset K$ and $h\in C(K,X)$ are such that $f(h(y)) = y$ for all $y\in A$ then there is a continuous right  inverse $g$ of $f$ on $K$ that satisfies
  $$
    g(y) = h(y),\quad \forall y\in A,
  $$
  and
  \begin{equation}
     \label{eq:rinv-est}
     \|g - h\|_\infty \le 2M \max _{y\in K} \| f(h(y)) - y\|.
  \end{equation}
\end{prop}

\begin{proof}
Consider the following measure
$$
  \mu: C(K,X) \to \R^+
$$
of how much a given function $g$ differs from a right inverse of $f$:
$$
  \mu(g) := \max_{y\in K} \| f(g(y)) - y\|.
$$
It is clear that $\mu$ is lower semicontinuous. (It is easy to check that it is continuous but we do not need this.)

The claim  is that there exists $g$ such that $\mu(g) = 0$.

In order to check the condition of Lemma~\ref{lem-ek-tem}, fix $\hat g\in C(K,X)$ such that
$$
  \mu(\hat g) > 0.
$$
Define $u\in C(K, X)$ by
$$
  u(y) := y - f(\hat g(y)).
$$
By definition,
$$
   \mu (\hat g) = \| u \|_\infty.
$$
So, $u$ is not identically equal to zero, because $\mu(\hat g)>0$.

Put
$$
  w( y) :=  [f'(\hat g(y))]^{-1} u(y).
$$
From \eqref{eq:3} it  is clear that $w\in C(K,X)$.

Therefore,  for $t > 0$
$$
  g_t := \hat g + tw \in C(K,X).
$$
Note for future reference that form \eqref{eq:2} it follows that $\|w\|_\infty \le M \|u\|_\infty$, that is
\begin{equation}
  \label{eq:norm-w}
  \|w\|_\infty \le M\mu(\hat g).
\end{equation}


Our next aim is to estimate $\mu(g_t)$. By definition
$$
   \mu(g_t) := \max_{y\in K} \| f(g_t(y)) - y\|.
$$
For $y\in K$ define $\varphi _y: \R^+\to \R^+$ by
$$
  \varphi _y(t) := \| f(g_t(y)) - y\|,
$$
hence
\begin{equation}
 \label{eq:mufi}
 \mu(g_t) := \max_{y\in K} \varphi _y(t).
\end{equation} 
Because the set $ \hat g(K)$ is compact and the set $w(K)$ is bounded, from Lemma~\ref{lem:uniform-on-compact} it follows that
$$
  \max_{y\in K}\| f(\hat g(y) +tw(y)) -  f(\hat g(y)) - t f'(\hat g(y))w(y)\| = \alpha(t)t,
$$
where $\alpha(t)\to 0$ as $t\to 0$.
But
$$
   f'(\hat g(y))w(y) =  f'(\hat g(y))  [f'(\hat g(y))]^{-1} u(y) = u(y),
$$
so
$$
   \|f(g_t(y)) -  f(\hat g(y)) - tu(y)\|_\infty =\alpha(t)t.
$$
Therefore, for any $y\in K$
\begin{eqnarray*}
 \varphi _y(t) &=& \| f(g_t(y)) - y\| \\
   &\le&  \|f(\hat g(y)) + tu(y)-y\| +  \|f(g_t(y)) -  f(\hat g(y)) - tu(y)\|\\
   &\le& \|(t-1)u(y)\| + \alpha(t)t.
\end{eqnarray*}
Since $\varphi_y(0) = \|u(y)\|$ we have that for small $t$
$$
  \varphi _y(t) \le (1-t) \varphi _y (0) + \alpha(t)t.
$$
Taking a maximum over $y\in K$, see \eqref{eq:mufi}, we get
$$
   \mu(g_t) \le (1-t) \mu(g_0)+ \alpha(t)t,
$$
or, in other words,
\begin{equation}
  \label{eq:diff}
  \mu( \hat g + tw) \le\mu(\hat g) - t \mu(\hat g) + \alpha(t)t.
\end{equation}
Since $\mu(\hat g) > 0$, for some $\delta > 0$ we then have $|\alpha(t)|< \mu(\hat g)/2$ for $ t\in (0,\delta)$. So,
$$
  \mu( \hat g + tw) < \mu(\hat g) - (t/2)\mu(\hat g), \quad \forall t\in (0,\delta).
$$
From \eqref{eq:norm-w}, which is $ \mu(\hat g) \ge (1/M)\|w\|_\infty$, we get
$$
  \mu( \hat g + tw) < \mu(\hat g) - (1/2M)\|tw\|_\infty, \quad \forall t\in (0,\delta),
$$
and we can apply Lemma~\ref{lem-ek-tem} with $r=1/2M$, $x=\hat g$ and $y= \hat g +(\delta/2)w$, to conclude that $\mu$ vanishes somewhere.

If now we would start from $h$ then  Lemma~\ref{lem-ek-tem} produces a $g$ such that $\mu (g) = 0$ and 
$$
  \|g-h\|_\infty \le 2M\mu(h),
$$
which is \eqref{eq:rinv-est}.

If $f(h(y)=y$ on $A$ then we can do the above proof on the closed suset $T$ of $C(K,X)$ 
$$
   T := \{g\in C(K,X):\ g(y) = h(y),\ \forall y\in Y\}.
$$

It is clear that in this case when considering $\hat g\in T$ we will have that $u(y)=w(y) = 0$ for all $y\in A$, so $g_t$ is also in $T$, and everything else works the same way.
\end{proof}

\section{Proof of Theorem~\ref{thm:main}}
\label{sec:proof}
We need only to show that $f$ is injective. Fix arbitrary $a,b\in X$ such that $f(a)=f(b)$. By considering instead of $f$ the function
$$
  x \to f(x-b) - f(b)
$$
we may assume without loss of generality that
$$
  f(b) = b = 0.
$$

Let $K_t := f([0,t])$ for $t\in [0,1]$ and for $g\in C(K_t,X)$
$$
  \mu_t(g) := \max_{0\le s\le t} \|f(g(f(sa))) - f(sa)\|. 
$$
\textsc{Claim} For each $\varepsilon > 0$ there is $g\in C(K,X)$ such that $g(0)= 0$, $\mu_1(g) = 0$ and 
$$
   \|a - g(f(a)\| < \varepsilon.
$$
If this is true then, since $f(a)=0$ and $g(0)=0$, we have $\|a\| < \varepsilon$. That is, $a=0=b$ and we are done.

\textsc{Proof of the Claim:}

\begin{thebibliography}{99}

\bibitem{AE}
J.-P. Aubin and I. Ekeland,  \emph{Applied nonlinear analysis}, John Wiley \& Sons, New York, 1984, ISBN: 0-486-45324-3

\bibitem{Asen-book}
Asen L. Dontchev, \emph{Lectures on Variational Analysis}, 
Book Series: Applied Mathematical Sciences, Springer, 2021,  ISBN: 978-3-030-79910-6

\bibitem{doro} A. L. Dontchev and R. T. Rockafellar, \emph{Implicit Functions and Solution Mappings:
A View from Variational Analysis}, Series in Operations Research and Financial Engineering, Springer, 2014, ISBN: 978-1-4939-1037-3


\bibitem{ES}
I. Ekeland and E. S\'er\'e, A local surjection theorem, 2017,
https://project.inria.fr/brenier60/files/2011/12/Brenier.pdf



\bibitem{ioffe}  A. Ioffe, \emph{Variational Analysis of Regular Mappings:
Theory and Applications}, Springer Monographs in Mathematics, 2017, SBN: 978-3-319-64277-2

\bibitem{plastock} R. Plastock, Homeomorogisms between Banach spaces, Trans. Amer. Math. Soc., 200, 1974, 169--183.

\end{thebibliography}



\end{document}
