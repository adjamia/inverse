\documentclass[a4paper,12pt]{article}
\usepackage[utf8]{inputenc}
\usepackage{amsmath}
\usepackage{amssymb}

\usepackage{amsthm}
\usepackage{graphicx}
\graphicspath{ {./graphs/} }

\newtheorem{theo}{Theorem}
\newtheorem{prop}{Proposition}
\newtheorem{lem}{Lemma}
\newtheorem{cor}{Corollary}
\newtheorem*{defi}{Definition}

\def\id{\mathrm{Id}\,}
\def\dom{\mathrm{dom}\,}
\def\gr{\mathrm{Gr}\,}
\def\R{\mathbb{R}}
\def\ds{\displaystyle}
%opening
\title{ Hadamard Inverse Function Theorem for G\^ateaux Smooth Function}
\author{Milen Ivanov and Nadia Zlateva}

\begin{document}

\maketitle

\begin{abstract}
  We generalise Hadamard Global Inverse Function Theorem to the case of G\^ateaux smooth function.
\end{abstract}

\section{Introduction}

Let $X$ be a Banach space. Consider a function
$$
    f: X\to X.
  $$
Recall that its directional derivative $  f'(x;h)$ at the point $x$ in the direction $h$ is 
$$
    f'(x;h) := \lim_{t\to 0}\frac{f(x+th) - f(x)}{t}.
  $$
 The function $f$  is called G\^ateaux differentiable at $x\in X$ if there is a bounded linear $f'(x):X\to X$ such that
  $$
    f'(x;h) = f'(x)h.
  $$
  The function $f$ is called {\em G\^ateaux smooth}, denoted $f\in C_G^1$, if in addition the function
  $$
   (x,h) \to f'(x;h)
  $$
  is continuous.
  
  
  

The following Theorem, known as Hadamard Theorem, is well known for Fr\'echet smooth $f$. The aim of this work is to extend Hadamard Theorem to the case of G\^ateaux smooth function. Here is the exact statement.
\begin{theo}
  \label{thm:main}
 Let $X$ be a Banach space. Let $f\in C_G^1$ and $f'(x)$ be invertible for all $x$ and satisfying:
   \begin{equation}
    \label{eq:2}
    \|[f'(x)]^{-1}\| \le \frac{1}{r},\quad\forall x\in X,
  \end{equation}
  for some $r > 0$.


 Then   there is $g\in C^1_G$ which is $r^{-1}$-Lipschitz and such that
\begin{equation}
   \label{eq:g-is-inverse}
     g(f(x)) =f(g(x))=x,\quad\forall x\in X.
  \end{equation}
  Moreover,
  \begin{equation}
   \label{eq:inv-der}
   g'(f(x)) = [f'(x)]^{-1},\quad\forall x\in X.
  \end{equation}
\end{theo}
Note that the only new part in this Theorem is actually the fact that $f$ is injective and proving this makes for the bulk of the tough technical work below.

\section{Preliminaries}
\label{sec:prelim}
Recall that in a normed space $(X,\|\cdot\|)$ the Kuratowski distance between $\varnothing\neq A\subset B\subset X$ can be defined as
$$
  d(A,B) := \sup\{ d(x,A):\ x\in B\},
$$
where
$$
  d(x,A) := \inf \{\|x-a\|:\ a\in A\}.
$$

\begin{lem}
  \label{lem-tn-0}
  Let $K$ be a non-empty compact set of the normed space $(X,\|\cdot\|)$. If $\{A_n\}_{n=1}^\infty$ is a sequence of non-empty  nested subsets of $K$, that is,
  $$
     A_n \subset A_{n+1},\quad\forall n\in\mathbb{N},
  $$
   then
  $$
     \lim_{n\to\infty} d(A_n,A_{n+1}) = 0.
  $$
\end{lem}
\begin{proof}
 Assume that there are $r>0$ and $n_k\in\mathbb{N}$, $n_k <n_{k+1}$, $\forall k$ such that
 $$
    d(A_{n_k-1},A_{n_k}) > r,\quad\forall k\in\mathbb{N}.
 $$
 Since $d(A_{n_{k-1}},A_{n_k}) > d(A_{n_k-1},A_{n_k})$, we have
 $$
    d(A_{n_{k-1}},A_{n_k})  > r,\quad\forall k\in\mathbb{N}.
 $$
 So, there are $x_k\in A_{n_k}$ such that $d(x_k, A_{n_{k-1}}) > r$. But, since $x_m\in A_{n_{k-1}}$ for $m<k$, we have
 $$
   \|x_m - x_k\| > r, \quad\forall m < k,
 $$
 which in effect means that
 $$
   \|x_m - x_k\| > r, \quad\forall m \neq k,
 $$
 and there is no convergent subsequence of $\{x_k\}_{k=1}^\infty$. Contradiction.
\end{proof}

\begin{prop}
   \label{pro:extent}
   Let $L > 0$.
   Let $(X,\|\cdot\|)$ be a normed space and let
   $$
     f:X\to X
   $$
   be a continuous function.  Let $K\subset X$ be nonempty  and compact.

   Consider the family $\mathcal{B}$ consisting of couples $(C,g_C)$, where $C\subset K$ is nonempty and compact,  and $g_C:C\to X$ is $L$-Lipschitz and such that
   $$
     f(g(y)) = y,\quad\forall y \in C.
   $$
   For $(C_i,g_{C_i})\in\mathcal{B}$ we write
   $$
     (C_1,g_{C_1}) \prec(C_2,g_{C_2})\iff C_1\subset C_2\mbox{ and }g_{C_2}(y) = g_{C_1}(y),\ \forall y\in C_1.
   $$

   Assume that for each $(C,g_C) \in \mathcal{B}$ such that $C\subsetneqq K$, there is $(C',g_{C'}) \in \mathcal{B}$ such that $(C,g_C)\prec (C',g_{C'})$ and $C\subsetneqq C'$.

   Then, if $(A,g_A) \in \mathcal{B}$, there is $(K,g_{K}) \in \mathcal{B}$ such that
   $$
      (A,g_A) \prec (K,g_K).
   $$
\end{prop}

\begin{proof}
   Let $A_1=A$. If $(A_n,g_{A_n})\in\mathcal{B}$ is chosen and $A_n\subsetneqq K$, then let
   \begin{equation}
     \label{eq:def-tn}
     t_n: = \sup \{ d(A_n,A'): \ (A',g_{A'})\in\mathcal{B}\mbox{ and }(A_n,g_{A_n})\prec (A',g_{A'})\}.
   \end{equation}

   By assumption $t_n > 0$. Also, $t_n < \infty$, since $K$ is bounded.

   Take $(A_{n+1},g_{A_{n+1}})\in\mathcal{B}$ such that $(A_n,g_{A_n})\prec (A_{n+1},g_{A_{n+1}})$ and
   \begin{equation}
      \label{eq:an+1-choice}
      d(A_n,A_{n+1}) > t_n/2.
   \end{equation}

   If at some step $A_n= K$, we are done. Otherwise, by Lemma~\ref{lem-tn-0} we have $d(A_n,A_{n+1})\to 0$ as $n\to\infty$ and  \eqref{eq:an+1-choice} yields
   \begin{equation}
      \label{eq:tn-to-0}
      \lim _{n\to\infty} t_n = 0.
   \end{equation}

   Let
   $$
     D := \bigcup_{n=1}^\infty A_n,\quad g(y) = g_{A_n}(y),\mbox{ if }y\in A_n.
   $$
   It is easy to check that $g$ is well defined on $D\subset K$.

   Since $g$ is $L$-Lipschitz, it can be extended by continuity to the closure $C:=\overline{D}$. As $C\subseteq K$, it is compact. We denote this  $L$-Lipschitz extension by $g_{C}$. Since $f$ is continuous, $f(g_C(y))=y$ for all $y\in C$. In other words, $(C,g_C)\in\mathcal{B}$. Clearly, $(A_n,g_{A_n})\prec (C,g_{C})$ for all $n$.

   If $C \equiv K$ we are done. Otherwise by assumption there would be $(C',g_{C'})\in\mathcal{B}$ such that $(C,g_{C}) \prec (C',g_{C'})$ and $C\subsetneqq C'$. By the definition \eqref{eq:def-tn} of $t_n$ and since $A_n\subset C$, we have that
   $$
     t_n \ge d(A_n, C') \ge d(C,C'),\quad\forall n\in\mathbb{N}.
   $$
Since $ d(C,C')>0$ the latter contradicts   \eqref{eq:tn-to-0}.
\end{proof}

Next is a precursor to Ekeland Variational Principle, see \cite[Chapter 5, Section 1]{AE}. Of course, it easily follows from Ekeland Variational Principle itself, see e.g. \cite[Basic Lemma]{ioffe}. See also the comments concerning the ``Basic  Lemma'' on \cite[p. 93]{ioffe}. Here we present a proof based on what is called in these comments ``simple iteration''.
\begin{lem}
  \label{lem-ek-tem}
  Let $X$ be a Banach space and let $\mu:X \to \R^+\cup\{\infty\}$ be lower semicontinuous and such that for some $ r>0$
  $$
    \forall x:\ 0<\mu (x) < \infty\Rightarrow \exists y:\ \mu(y) < \mu(x) - r\|y-x\|.
  $$
  Then for each $x\in \dom \mu$ there is $y\in X$ such that
  $$
     \mu(y) = 0\mbox{ and }r\|y-x\| \le \mu(x).
  $$
\end{lem}
\begin{proof}
  Fix $x_0\in \dom \mu$ such that $\mu(x_0) > 0$.  Let $x_1,x_2,\ldots,x_n$ be already chosen in the following way.


  Let
  \begin{equation}
   \label{eq:nu-def}
     \nu_n= \sup \{ \|x-x_n\|:\ \mu(x) < \mu(x_n) - r\|x-x_n\|\}.
  \end{equation}
  We are given that the set in the right hand side is nonempty, so $\nu_n > 0$. Also, since $\mu\ge 0$, we have that $\nu_n \le  \mu(x_n)/r <\infty$.

  Choose a $x_{n+1}$ such that
  \begin{equation}
   \label{eq:x-n+1-def}
     \mu(x_{n+1}) < \mu(x_n) - r\|x_{n+1}-x_n\|
     \mbox{ and }\|x_{n+1}-x_n\| > \nu_n/2.
  \end{equation}
  Note that
  $$
    \|x_{n+1} - x_0\| \le \sum_{i=0}^n \|x_{i+1}-x_i\|\le \sum_{i=0}^n(\mu(x_i)-\mu(x_{i+1}))/r\le\mu(x_0)/r,
  $$
  so: if $\mu(x_{n+1})=0$, we are done. If not, we continue by induction.

  If we would end up with an infinite sequence $(x_n)_0^\infty$, then from the above inequality $\sum_{i=0}^\infty \|x_{i+1}-x_i\| \le\mu(x_0)/r$, so $x_n\to \bar x$ as $n\to\infty$ and $\|\bar x- x_0\|\le \mu(x_0)/r$.

  If $\mu(\bar x) > 0$ then we can find $\bar y$ such that $\mu(\bar y) < \mu(\bar x) - r\|\bar y-\bar x\|$. Since $\mu$ is lower semicontinuous, we will have for all $n$ large enough $\mu(\bar y) < \mu(x_n) - r\|\bar y-x_n\|$. That is, see \eqref{eq:nu-def},  $\nu_n \ge \|\bar y-x_n\| \ge  \|\bar y-\bar x\|/2 > 0$ for all $n$ large enough. But on the other hand from \eqref{eq:x-n+1-def} it follows that $\nu_n\to 0$, contradiction.

  So,  $\mu(\bar x) = 0$ and we are done.
\end{proof}

We make a crucial use of the following Theorem of Ekeland, \cite{ek:article}.
\begin{theo}
 \label{thm:ekeland-surj}
  Let $X$ be a Banach space. Let $f:X\to X$ be continuous and G\^ateaux differentiable and satisfying \eqref{eq:2}. Then
  \begin{equation}
     \label{eq:open-linear}
     f(B^\circ(x;t)) \supset B^\circ(f(x);rt),\quad\forall x\in X,\forall t > 0.
  \end{equation}
\end{theo}
\begin{proof}
We can assume without loss of generality that $f(0)=0$.

 Fix a $\bar y \in X$ such that $\|\bar y\| < rt$ and consider
 $$
   \mu(x) := \|f(x) - \bar y\|.
 $$

 Fix $a\in (0,1)$ such that $\|\bar y\|  < art$.

 If $\mu(x) >0$, that is $f(x)\neq \bar y$, then $h:=[f'(x)]^{-1}(\bar y - f(x))\neq 0$. Since
 \begin{eqnarray*}
   f(x+th) &=& f(x) + tf'(x)h + o(t)\\
                 &=& f(x) + t(\bar y -f(x)) + o(t),
 \end{eqnarray*}
 we have that
  \begin{eqnarray*}
    \mu(x+th) &=& \|f(x+th) - \bar y\|\\
                        &=& (1-t)\mu(x) + o(t)\\
                        &<& (1-at)\mu(x), \quad t\in (0,\delta)
  \end{eqnarray*}
  for some $\delta>0$.

  Since $\ds \|h\| = \|[f'(x)]^{-1}(\bar y - f(x))\| \le \|[f'(x)]^{-1}\| \|\bar y - f(x)\| \le \frac{1}{r}\mu(x)$, we have
  $$
     \mu(x+th) < \mu(x) - ar\|th\|
  $$
  for all $t\in (0,\delta)$. That is, $\mu(y) < \mu(x) - ar\|y-x\|$ for $y = x+th$. From Lemma~\ref{lem-ek-tem} it follows that there is $\bar x$ such that $\mu(\bar x) = 0$ and $\|\bar x\| \le \frac{1}{ar}\|\bar y\|$. But the latter is smaller than $t$.

  In short, we have proved that for each $\bar y$ such that  $\|\bar y\| < rt$  there is $\bar x$ such that $\|\bar x \| < t$ and $f(\bar x) = \bar y$.
\end{proof}

\section{Local Injectivity}
  \label{sec:local-inj}
  \begin{lem}
     \label{lem:approx-on-compact}
     Let $f:X\to X$ be $C_G^1$-smooth. Let $K$ be a nonempty convex and compact subset of $X$.  Then for any $\varepsilon > 0$ there exists $\delta > 0$ such that
     \begin{equation}
        \label{eq:lagrange-on-K}
        \|f(y) - f(x) - f'(a)(y-x)\| \le \varepsilon\|y-x\|,\quad\forall a\in K,\ \forall x,y\in K\cap B(a;\delta).
     \end{equation}
  \end{lem}
  \begin{proof}
  Fix a $\varepsilon > 0$.
  
   Let
   $$
     K' := K - K = \{x-y:\ x,y\in K\}.
   $$
   
   it is clear that $K'$ is convex and compact. Therefore, there exists $\delta > 0$ such that
   \begin{equation}
     \label{eq:aoc-1}
     \|f'(x;h) - f'(y;h)\| < \varepsilon,\quad \forall x,y\in K:\ \|x-y\|\le \delta,\ \forall h\in K'.
   \end{equation}
   Indeed, if this were not the case, we could find secuences $(x_n)_1^\infty,(y_n)_1^\infty\subset K$ with $\|x_n-y_n\|\to 0$ and $(h_n)_1^\infty\subset K'$ such that 
   $$
     \|f'(x_n;h_n) - f'(y_n;h_n) \| \ge \varepsilon.
   $$
   From the compactness of $K$ and $K'$ we may assume without loss of generality that all the three sequences are convergent. Because $\|x_n-y_n\|\to 0$, the first two must converge to the same point. Let $x_n \to a$, $y_n \to a$ and $h_n\to \bar h$ as $n\to\infty$. From the continuity of $(x,h)\to f'(x;h)$ we have that $ f'(x_n;h_n) \to f'(a;\bar h)$ and $ f'(y_n;h_n) \to f'(a;\bar h)$; and the above inequality implies $0 = \| f'(a;\bar h)- f'(a;\bar h)\| \ge \varepsilon$ which is absurd.

    Fix now any $a\in K$ and $x,y\in K\cap B(a;\delta)$, so $ \|x-y\|\le 2\delta$. Let $\varphi:[0,1]\to X$ be
    $$
       \varphi (t) := f(x + t(y-x)).
    $$
    Then 
    $$
      \varphi' (t) = f'(x + t(y-x);y-x).
    $$
    From convexity $x + t(y-x)\in  K\cap B(a;\delta)$ for all $t\in[0,1]$ and, since $y-x\in K'$ by definition, \eqref{eq:aoc-1} gives $\|\varphi'(t)-f'(a;y-x)\| < \varepsilon$ for all $t\in [0,1]$. We use Neuton-Leibnitz Theorem to estimate
    \begin{eqnarray*}
       \|f(y) - f(x) - f'(a)(y-x)\| &=& \| \varphi(1) - \varphi(0) - f'(a;y-x)\| \\
            &=& \| \int_0^1\varphi'(t)\, dt - \int_0^1 f'(a;y-x) \, dt\| \\
            &\le& \int_0^1\| \varphi'(t) - f'(a;y-x)\|\, dt \\
            &<& \varepsilon.
    \end{eqnarray*}
  But we are missing $\|x-y\|$. :(
  \end{proof}


  
  
  \begin{prop}
     \label{pro:local-inj-on-compact}
     Let $f:X\to X$ be $C_G^1$-smooth and satisfying \eqref{eq:2}. Let $K$ be a nonempty convex and compact subset of $X$. Then there is $\delta > 0$ such that $f$ is injective on $B(x;\delta)$ for each $x\in K$.
  \end{prop}


\section{Proof of Theorem~\ref{thm:main}}

Let $X$ be a separable Banach space and let $f:X\to X$ be $C^1$-smooth and satisfying \eqref{eq:2}. Assume that
$$
  f(0) = 0.
$$

We sill be using small letters for functions, or singlevalued mappings, and caps for multivalued mappings. For $G_1$ and $G_2$ we wite $G_1\subset G_2$ if $\gr G_1 \subset \gr G_2$.

Define (and fix) $G:X \rightrightarrows X$ by
\begin{equation}
  \label{eq:def-G}
  G(y) := \{x\in X:\ f(x) = y\}.
\end{equation}
From Ekeland's Surjectivity Theorem~\ref{thm:ekeland-surj} it follows that if $x_0\in G(y_0)$, that is, $f(x_0)=y_0$, then
  \begin{equation}
     \label{eq:aubin}
     G(y) \cap B(x_0; \frac{1}{r}\|y-y_0\|)  \neq\varnothing,\quad\forall y\in X.
  \end{equation}

  We will now quantify to our case the neighbourhoods in Theorem~\ref{thm:inverse-classical}.

  \begin{lem}
    \label{lem:uniqueness-explicit}
    Let $f(x_0) = y_0$ and let $\delta > 0$ be such that
    \begin{equation}
      \label{eq:osc}
      \|f'(a)-f'(b)\| < r,\quad\forall a,b\in B^\circ(x_0;\delta).
    \end{equation}
    Then on $V:=B^\circ(y_0;r\delta)$
    \begin{equation}
      \label{eq:g_V}
      G(y)\cap B^\circ (x_0;\delta) = \{g_V(y)\},\quad\forall y\in V,
    \end{equation}
   where $g_V:V\to X$ is a $r^{-1}$-Lipschitz function.
  \end{lem}
  \begin{proof}
    We can assume without loss of generality that $x_0=y_0=0$.

    From \eqref{eq:aubin} it follows that $G(y)\cap \delta B^\circ _X\neq\varnothing$ for all $y\in V= r\delta B^\circ _X$. To prove that the intersection is a singleton is the same as proving that $f$ is injective on $\delta B^\circ _X$ and this is done the standard way.

    Fix $a\neq b\in \delta B^\circ _X$ and let $h := b-a$. Consider $\varphi(t) := f(a+th)$ for $t\in[0,1]$. We have
    $$
       \varphi'(t) := f'(a+th)h.
    $$
    From \eqref{eq:osc} it follows that $\|\varphi'(t)-\varphi'(0)\| < r\|h\|$.
    \begin{eqnarray*}
       \|\varphi(1)-\varphi(0)-\varphi'(0)\| &=&\big\|\int_0^1(\varphi'(t) - \varphi'(0))\, dt\big\| \\
       &\le& \int_0^1 \|\varphi'(t) - \varphi'(0)\|\, dt < r\|h\|.
    \end{eqnarray*}
   But from \eqref{eq:2} we have $\|h\| = \|[f'(a)]^{-1}f'(a)h\| \le r^{-1}\|f'(a)h\|$. So, $\|\varphi'(0)\| \ge  r\|h\|$ and $\varphi(1)-\varphi(0)\neq 0$, that is, $f(a)\neq f(b)$.

   From \eqref{eq:2} and \eqref{eq:inv-der} it follows that $\|g_V'\| \le r^{-1}$ and since $V$ is convex, $g_V$ is  $r^{-1}$-Lipschitz by Mean Value Theorem.
  \end{proof}



The greater part of the work below will be to show that there is $r^{-1}$-Lipschitz $g:X\to X$ such that $g(0)=0$ and $g\subset G$. We need this because of the following
\begin{lem}
  \label{lem:inj-0}
  If there is continuous $g:X\to X$ such that
  $$
    g(0)=0\mbox{ and }g\subset G,
  $$
  then
  $$
    f(x)\neq 0,\quad\forall x\neq 0.
  $$
\end{lem}
\begin{proof}
 Fix an arbitrary $\bar x \neq 0$ and let
 $$
   \varphi (t) := f(t\bar x),\quad t\in[0,1].
 $$
 Define
 $$
   I:= \{ t\in [0,1]:\ g(\varphi(t)) = t\bar x\}.
 $$
 It is clear that $0\in I$, so the latter is nonempty. Also, $I$ is closed by continuity.

 Let $\bar t := \max I$.

 Assume that $\bar t < 1$. From Lemma~\ref{lem:uniqueness-explicit} there are open neighbourhoods $U$ of $\bar t \bar x$ and $V$ of $\varphi(\bar t)$ such that $G\cap U$ is single-valued on $V$. Since $g\subset G$ this implies
 $$
    G(y) \cap U = \{g(y)\},\quad\forall y\in V.
 $$
 Since $\varphi$ is continuous, $t\bar x\in U$ and $\varphi(t)\in V$ for $t\in (\bar t,\bar t+\delta)$ for some $\delta>0$. Since $t\bar x \in G(\varphi (t))$, because of \eqref{eq:def-G} and $f(t\bar x) =\varphi(t)$, we have that $t\bar x \in G(\varphi (t))$. But $G(\varphi (t)) = g(\varphi (t))$ for  $t\in (\bar t,\bar t+\delta)$, so
 $g(\varphi (t)) = t\bar x$, that is, $t\in I$, for some $t>\bar t$, contradicition.

 So, $1\in I$, that is, $g(f(\bar x)) = \bar x$.

 If $f(\bar x) = 0$ then $ \bar x = g(f(\bar x)) = g(0) = 0$, contradicition. Therefore,
 $$
    f(\bar x)\neq 0.
 $$
\end{proof}

\begin{lem}
  \label{lem:lem-66}
  Let $A\subset X$ be a non-empty convex and compact set. Assume that  there is $\ds \frac{1}{r}$-Lipschitz  $g_A:A\to X$ such that
  $g_A\subset G$, that is
    $g_A(y) \subset G(y)$ for all $y\in A$.

 Then there is $\varepsilon >0$ such that on $D:=A+\varepsilon B_X$ there is $\ds \frac{1}{r}$-Lipschitz $g_D:D\to X$ such that
 $$
   g_A\subset g_D\subset G,
$$
that is, $g_D(y)=g_A(y)$, for all $y\in A$ and $ g_D(y)\in G(y)$, for all $y\in D$.
\end{lem}
\begin{proof}
Since $g_A(A)$ is compact, from Lemma~\ref{lem:pfu} there is $\delta >0$ such that for any $x\in g_A(A)$ and for all $a,b\in B^\circ(x;\delta)$ we have $\| f'(a)-f'(b)\|<r$. From Lemma~\ref{lem:uniqueness-explicit} if follows that for each $u=g_A(v)$, $v\in A$ there is a $\ds \frac{1}{r}$-Lipschitz function
$$
  g_{B^\circ(v;r\delta)}: B^\circ(v;r\delta) \to X
$$
such that
\begin{equation}
   \label{eq:5lychka}
   G(y) \cap B^\circ (u;\delta) = \{g_{B^\circ(v;r\delta)}(y)\},\quad\forall y\in B^\circ(v;r\delta).
\end{equation}
Note for future reference that the above, $g_A(v)=u$ and $g_A\subset G$ imply in particular that
\begin{equation}
  \label{eq:g-u-v}
  g_{B^\circ(v;r\delta)}(v) = u.
\end{equation}
Let
\begin{equation}
  \label{eq:eps-def}
  \varepsilon := {r\delta \over 8}.
\end{equation}
\textsc{Claim.} If $y\in B^\circ(v_1;\varepsilon)\cap B^\circ(v_2;\varepsilon)$ for some $v_i\in A$ then
\begin{equation}
 \label{eq:zvezda}
 g_{B^\circ(v_1;r\delta)}(y) = g_{B^\circ(v_2;r\delta)}(y).
\end{equation}

Indeed, fix $y\in B^\circ(v_1;\varepsilon)\cap B^\circ(v_2;\varepsilon)$ and let
$$
  x_i = g_{B^\circ(v_i;r\delta)}(y),\quad i = 1,2.
$$
We will show that $x_1 = x_2$.

To this end note that
$$
  \|x_2 - u_1\| \le \|x_2 - u_2\| + \|u_2 - u_1\|,
$$
and
$$
  \|u_2 - u_1\| = \|g_A(v_2) - g_A(v_1)\| \le {1 \over r}\|v_2 - v_1\| < { 2\varepsilon \over r} < {\delta\over 2},
$$
and, using \eqref{eq:g-u-v},
\begin{eqnarray*}
  \|x_2 - u_2\| &=& \|g_{B^\circ(v_2;r\delta)}(y)  - g_A(v_2) \|\\
  &=&  \|g_{B^\circ(v_2;r\delta)}(y)  - g_{B^\circ(v_2;r\delta)}(v_2) \|\\
  &\le& {1\over r}\|y-v_2\| < {\varepsilon\over r} < {\delta \over 2}.
\end{eqnarray*}
So, $\|x_2 - u_1\| < \delta$ and because $x_2\in G(y)$, \eqref{eq:5lychka} implies
$$
  x_2\in G(y) \cap B^\circ(u_1,\delta) = \{g_{B^\circ(v_1;r\delta)}(y)\} = \{x_1\},
$$
that is, $x_2 = x_1$. The Claim is established.

Now, \eqref{eq:zvezda} implies that the following definition is consistent
$$
   g_D(y) :=  g_{B^\circ(v;r\delta)}(y)\mbox{ if }y\in B(v;\varepsilon).
$$
Locally the function $g_D$ coincides with those coming from the Inverse Function Theorem~\ref{thm:inverse-classical}, so the norms of its directional derivatives are bounded by $\ds {1 \over r}$, and thus, since $D$ is convex, $g_D$ is $\ds {1 \over r}$-Lipschitz by Mean Value Theorem.
\end{proof}

\begin{lem}
  \label{lem:lem-78}
Let $A$ and $K$ be non-empty convex compact subsets of $X$ such that $A\subset K$. If $g_A:A\to X$ is $\ds \frac{1}{r}$-Lipschitz function such that $g_A\subset G$, then there is $\ds \frac{1}{r}$-Lipschitz function $k_K:K\to X$ such that $g_A\subset g_K\subset G$.
\end{lem}

\begin{proof}
We will apply Proposition~\ref{pro:extent}. Define $\cal{B}$, see Proposition~\ref{pro:extent}, to consists of all couples $(C,g_C)$, where $C\subset K$ is non-empty convex compact and $g_C:C\to X$ is $\ds \frac{1}{r}$-Lipschitz function such that $g_C\subset G$. Note that 
\[
(C_1,g_1)\prec (C_2,g_2) \Leftrightarrow g_{C_1}\subset g_{C_2}.
\]

Fix $(C,g_C)\in {\cal B}$ such that $C\subsetneqq K$. From Lemma~\ref{lem:lem-66} applied for $A=C$, there is $\varepsilon >0$ such that on $D:=C+\varepsilon B_X$ there is $g_G:D\to X$ such that $g_C\subset g_D\subset G$. Let $C':=D\cap K$ and $g_{C'}=g_D \restriction_{C'}$. Since $D$ is closed and convex, $C'$ is a convex compact subset of $K$ which is non-empty because it contains $C$. Since $g_{C'}\subset g_{D}$, we have $g_C\subset g_{C'}\subset G$, that is $(C',g_{C'})\in {\cal B}$ and $(C,g_C)\prec (C',g_{C'})$.

Take $a\in C$ and $b\in K\setminus C$. From convexity, $[a,b]\cap C=[a,\xi]$, where $\xi\neq b$. Of course, $[a,b]\subset K$. Also, because for any $x\in K$ such that $\| x-\xi\|\le \varepsilon$ it holds that $x\in C'$, for $t>0$ and small enough $\xi +t(b-a)\in C'\setminus C$. That is, $C\subsetneqq C'$. We have verified all assumptions of Proposition~\ref{pro:extent}, so there is $(K,g_K)\in {\cal B}$.
\end{proof}

\begin{lem}\label{lem:8}
There is $\ds \frac{1}{r}$-Lipschitz function $g:X\to X$ such that $g(0)=0$ and $g\subset G$.
\end{lem}

\begin{proof}
Let $y_0=0$, and $K_0=\{0\}$. Let $\{y_n\}_{n=0}^\infty$ be a dense sequence such that $\overline{\{y_n\}_{n=0}^\infty}=X$. Let $K_n=\overline{co}\{ y_i;i=0\dots n\}$. So, $K_n$ are convex compact and nested $K_n\subset K_{n+1}$, $\forall n\ge 0$, and $\displaystyle \overline{\cup_{n\ge 0}K_n}=X$.

Define $g_{K_0}:K_0\to X$ by $g_{K_0}(0)=0$. If $g_{K_0},\dots,g_{K_n}$ are defined so that $g_{K_i}:K_i\to X$  are $\ds \frac{1}{r}$-Lipschitz, $g_{K_i}\subset g_{K_{i+1}}$ and $g_{K_i}\subset G$, for all $i=0,\dots,n$, we can apply Lemma~\ref{lem:lem-78} to $K_n$ and $K_{n+1}$ to get a $\ds \frac{1}{r}$-Lipschitz function $g_{K_{n+1}}:K_{n+1}\to X$ such that $g_{K_n}\subset g_{K_{n+1}}\subset G$. By induction we get a sequence of $\ds \frac{1}{r}$-Lipschitz functions $\{ g_{K_n}\}_n=1^\infty$ such that $g_{K_n}$ is defined on $K_n$ and 
\begin{equation}\label{eq:ugugu}
g_{K_n}\subset g_{K_{n+1}}\subset G,\quad \forall n\ge 0.
\end{equation}

Let $\displaystyle W:=\cup_{n=0}^\infty K_n$, so $\overline W=X$. Define $\hat g:W\to X$ by $\hat g(y):=g_{K_n}(y)$ if $y\in K_n$. The definition is correct by \eqref{ugugu}. Of course, $\hat g$ is $\ds \frac{1}{r}$-Lipschitz on $W$, so it can be uniquely extended to $\ds \frac{1}{r}$-Lipschitz $g:X\to X$ such that $\hat g\subset g$. Now, if $z_n\in W$ and $z_n\to y$ as $n\to\infty$, we have $f(g(z_n))=z_n$ and by continuity, $f(g(y))=y$, that is $g\subset G$.
\end{proof}

Now we are ready to round up the proof of the separable case of Theorem~\ref{thm:main}. Indeed, we know, see e.g. Lemma~\ref{lem:uniqueness-explicit}, that $f$ is surjective, so we only need to show that it is injective. Take $x_1$ and $x_2$ such that $f(x_1)=f(x_2)$. By considering instead of $f$ the function $x\to f(x_2-x)-f(x_2)$ we can assume without loss of generality that $x_2=0$ and $f(0)=0$. From Lemma~\ref{lem:8} it follows that there is  $\ds \frac{1}{r}$-Lipschitz  $g$ such that $g(0)=0$ and $d\subset G$. From Lemma~\ref{lem:inj-0} we get $x_1=0$, that is $x_1=x_2$ and $f$ is injective.

\begin{thebibliography}{99}

\bibitem{AE}
J.-P. Aubin and I. Ekeland,  \emph{Applied nonlinear analysis}, John Wiley \& Sons, New York, 1984, ISBN: 0-486-45324-3

\bibitem{Asen-book}
Asen L. Dontchev, \emph{Lectures on Variational Analysis}, 
Book Series: Applied Mathematical Sciences, Springer, 2021,  ISBN: 978-3-030-79910-6

\bibitem{doro} A. L. Dontchev and R. T. Rockafellar, \emph{Implicit Functions and Solution Mappings:
A View from Variational Analysis}, Series in Operations Research and Financial Engineering, Springer, 2014, ISBN: 978-1-4939-1037-3

 \bibitem{ek:article} Ekeland's article
 
\bibitem{ES}
I. Ekeland and E. S\'er\'e, A local surjection theorem, 2017,
https://project.inria.fr/brenier60/files/2011/12/Brenier.pdf



\bibitem{ioffe}  A. Ioffe, \emph{Variational Analysis of Regular Mappings:
Theory and Applications}, Springer Monographs in Mathematics, 2017, SBN: 978-3-319-64277-2

\bibitem{plastock} R. Plastock, Homeomorogisms between Banach spaces, Trans. Amer. Math. Soc., 200, 1974, 169--183.

\end{thebibliography}


\end{document}
